PREFIX=platform
ARTIFACT=$(shell basename $(PWD))
# version is defined in the build.properties file.
VERSION=$(version)

guard-%:
	@ if [ "${${*}}" = "" ]; then \
        echo "Environment variable $* not set"; \
        exit 1; \
	fi

default: build

build: guard-AWS_ECR guard-BUILD_NUMBER guard-version
	docker build --no-cache \
		-t "${AWS_ECR}/$(PREFIX)/$(ARTIFACT):latest" \
		-t "${AWS_ECR}/$(PREFIX)/$(ARTIFACT):$(VERSION)" \
		-t "${AWS_ECR}/$(PREFIX)/$(ARTIFACT):$(VERSION).${BUILD_NUMBER}" .

publish: guard-AWS_ECR guard-BUILD_NUMBER guard-version
	docker push "${AWS_ECR}/$(PREFIX)/$(ARTIFACT):latest"
	docker push "${AWS_ECR}/$(PREFIX)/$(ARTIFACT):$(VERSION)"
	docker push "${AWS_ECR}/$(PREFIX)/$(ARTIFACT):$(VERSION).${BUILD_NUMBER}"

ci: build publish