import os
import pymysql.cursors

"""
This would be an example of a file managing the connection to a MySQL database. MySQL is not necessarily the tool
that will be used in the long run. But the idea should be the same.
"""

dbhost = None
dbuser = None
dbpassword = None

if os.getenv('MYSQL_HOST'):
    dbhost = os.getenv('MYSQL_HOST')
if os.getenv('MYSQL_USER'):
    dbuser = os.getenv('MYSQL_USER')
if os.getenv('MYSQL_PASSWORD'):
    dbpassword = os.getenv('MYSQL_PASSWORD')

connection = pymysql.connect(host=dbhost,
                             user=dbuser,
                             password=dbpassword,
                             db='database',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
