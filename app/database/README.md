### Database

This package should house any SQL or ORM files for the Identity Service database.

Any actual SQL should go in the `sql` directory.