# get java
apt-get update
apt-get -y install default-jre
java -version || exit 1
# get the liquibase executable
wget https://github.com/liquibase/liquibase/releases/download/liquibase-parent-3.8.0/liquibase-3.8.0-bin.tar.gz -O database/liquibase/liquibase.tar.gz
# extract
tar -C database/liquibase/ -xpzf database/liquibase/liquibase.tar.gz
# get the mysql connector
wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java_8.0.17-1debian9_all.deb -O database/liquibase/connector.deb
# install connector
dpkg -i database/liquibase/connector.deb
