import os
import subprocess
from service import global_secrets


def migrate():
    host = os.getenv('DATABASE_HOST')
    port = os.getenv('DATABASE_PORT')
    user = os.getenv('DATABASE_USERNAME')
    password = global_secrets.get(os.getenv('DATABASE_PASSWORD_KEY'))
    schema = os.getenv('DATABASE_NAME')

    '''
    database/liquibase/liquibase
        --changeLogFile=database/liquibase/change_log.xml \
        --username=${DATABASE_USERNAME} \
        --password=${DATABASE_PASSWORD} \
        --url=jdbc:mysql://${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}?createDatabaseIfNotExist=true&serverTimezone=UTC \
        --classpath=/usr/share/java/mysql-connector-java-8.0.17.jar \
        update
    '''
    liquibase_command = f'{os.getenv("LIQUIBASE_LOCATION", "database/liquibase/liquibase")}'
    change_log_path = f'{os.getenv("CHANGE_LOG_PATH", "database/liquibase/change_log.xml")}'
    class_path = f'{os.getenv("LIQUIBASE_CLASS_PATH", "database/liquibase/mysql-connector-java-8.0.17/mysql-connector-java-8.0.17.jar")}'

    if os.getenv('SKIP_MIGRATIONS', 'false').lower() == 'false':
        process = subprocess.run([liquibase_command,
                                  f'--changeLogFile={change_log_path}',
                                  f'--username={user}',
                                  f'--password={password}',
                                  f'--url=jdbc:mysql://{host}:{port}/{schema}?createDatabaseIfNotExist=true&serverTimezone=UTC',
                                  f'--classpath={class_path}',
                                  f'update'])
        return process.returncode
    else:
        return 0
