import os
from sys import exit
import json
import threading

from flask import make_response, render_template, abort, request
from jinja2 import TemplateNotFound

from config import title, description
from service.api.v1 import get_blueprint as get_apiv1

from service import app, logger
from database import migrate


app.register_blueprint(get_apiv1(title=title, description=description))

migration_complete = False
migration_failed = False

@app.before_first_request
def activate_job():
    def run_job(lock):
        from database import migrate
        global migration_failed
        global migration_complete
        try:
            status = migrate()
        except Exception as e:
            logger.error(e)
            with lock:
                migration_failed = True
            exit(1)

        with lock:
            if not status:
                logger.info('migration successful')
                migration_complete = True

            else:
                logger.info('migration failed')
                migration_failed = True

    global migration_complete
    if not migration_complete:
        lock = threading.Lock()
        thread = threading.Thread(target=run_job, args=(lock,))
        thread.start()


@app.route('/')
def get():
    blueprints = app.blueprints
    try:
        return render_template('index.html', blueprints=blueprints, title=title)
    except TemplateNotFound:
        abort(404)


@app.route('/admin/health', strict_slashes=False)
def health():
    global migration_failed
    if migration_failed:
        shutdown_server()
    response = make_response(json.dumps({'status': 'ok'}), 200)
    return response


@app.route('/admin/ready', strict_slashes=False)
def ready():
    global migration_complete
    global migration_failed

    if migration_complete:
        response = make_response(json.dumps({'status': 'ok'}), 200)
    else:
        response = make_response(json.dumps({'status': 'migration pending'}), 404)
    if migration_failed:
        shutdown_server()
    return response


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


@app.route('/admin/shutdown', methods=['POST'])
def shutdown():
    shutdown_server()
    return 'Server shutting down...'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
