import os
import logging
from flask import Flask


logging.basicConfig(format='%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-4s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger(__name__)

if os.getenv('SKIP_VAULT'):
    local_secrets = dict(populate_env_vars='in_get_secrets')
    logger.setLevel('DEBUG')
else:
    local_secrets = dict()
    logger.setLevel('INFO')

app = Flask(__name__)

from .secrets import get_secrets
global_secrets = get_secrets(local_secrets)
