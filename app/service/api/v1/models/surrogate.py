from flask_restplus import fields


def generate(api):
    return api.model('Surrogate', {
        'sourceSystemSlug': fields.String(required=True, description='slug of the source system from which the surrogate is for'),
        'sourceSystemRecordId': fields.String(required=True, description='id of the individual/consumer from the source system'),
        'fiId': fields.Integer(required=True, description='id of the financial institution at Kasasa, use 0 if not applicable'),
        'fullName': fields.String(description='full name of the individual'),
        'firstName': fields.String(description='first name of the individual'),
        'lastName': fields.String(description='last name of the individual'),
        'middleName': fields.String(description='middle name of the individual'),
        'suffix': fields.String(description='suffix of the individual'),
        'address1': fields.String(description='street address of the individual'),
        'address2': fields.String(description='additional address information e.g. apartment number'),
        'city': fields.String(description='city'),
        'state': fields.String(description='state'),
        'zip': fields.String(description='zip code'),
        'zipExtended': fields.String(description='additional 4-digit suffix associated with zip code'),
        'email': fields.String(description='email of the individual'),
        'phone': fields.String(description='phone number for the individual'),
        'sourceSystemRecordCreatedDate': fields.DateTime(description='date and time at which the record was created'),
        'sourceSystemRecordLastUpdatedDate': fields.DateTime(description='date and time at which the record was last updated')
    })


def generate_response(api):
    return api.model('ResponseSurrogate', {
        'surrogateId': fields.Integer(description='id of the surrogate'),
        'sourceSystemSlug': fields.String(required=True, description='slug of the source system from which the surrogate is for'),
        'sourceSystemRecordId': fields.String(description='id of the individual/consumer from the source system'),
        'messages': fields.List(fields.String(), description='any noteworthy messages encountered')
    })


def generate_batch(api):
    batch_surrogate_fields = api.model('SurrogateInBatch', {
        'sourceSystemRecordId': fields.String(required=True, description='id of the individual/consumer from the source system'),
        'fiId': fields.Integer(required=True, description='id of the financial institution at Kasasa, use 0 if not applicable', default=-1),
        'fullName': fields.String(description='full name of the individual'),
        'firstName': fields.String(description='first name of the individual'),
        'lastName': fields.String(description='last name of the individual'),
        'middleName': fields.String(description='middle name of the individual'),
        'suffix': fields.String(description='suffix of the individual'),
        'address1': fields.String(description='street address of the individual'),
        'address2': fields.String(description='additional address information e.g. apartment number'),
        'city': fields.String(description='city'),
        'state': fields.String(description='state'),
        'zip': fields.String(description='zip code'),
        'zipExtended': fields.String(description='additional 4-digit suffix associated with zip code'),
        'email': fields.String(description='email of the individual'),
        'phone': fields.String(description='phone number for the individual'),
        'sourceSystemRecordCreatedDate': fields.DateTime(description='date and time at which the record was created', dt_format='rfc822'),
        'sourceSystemRecordLastUpdatedDate': fields.DateTime(description='date and time at which the record was last updated', dt_format='rfc822')
    })
    return api.model('BatchSurrogate', {
        'jobId': fields.Integer(description='job id'),
        'jobType': fields.String(description='job type', default='surrogates'),
        'sourceSystemSlug': fields.String(required=True, description='slug of the source system from which the surrogate is for'),
        'status': fields.String(description='job status'),
        'datetimeCreated': fields.String(description='datetime the job was created'),
        'datetimeLastUpdated': fields.String(description='datetime the job was last updated'),
        'records': fields.List(fields.Nested(batch_surrogate_fields), default=[]),
        'messages': fields.List(fields.String(), description='any noteworthy messages encountered')
    })


def generate_batch_response(api):
    batch_surrogate_fields = api.model('SurrogateInBatchResponse', {
        'surrogateId': fields.Integer(required=True, description='id of surrogate'),
        'sourceSystemSlug': fields.String(required=True, description='id of the source system'),
        'sourceSystemRecordId': fields.String(required=True, description='id of the individual/consumer from the source system')
    })
    paging_fields = api.model('Paging', {'totalResults': fields.Integer()})
    return api.model('BatchSurrogateResponse', {
        'jobId': fields.Integer(description='job id'),
        'jobType': fields.String(description='job type'),
        'sourceSystemSlug': fields.String(description='slug of the source system from which the surrogate is for'),
        'status': fields.String(description='job status'),
        'datetimeCreated': fields.String(description='datetime the job was created'),
        'datetimeLastUpdated': fields.String(description='datetime the job was last updated'),
        'datetimeCompleted': fields.String(description='datetime the job was completed successfully'),
        'messages': fields.List(fields.String(), description='any noteworthy messages encountered'),
        'records': fields.List(fields.Nested(batch_surrogate_fields), default=None),
        'paging': fields.Nested(paging_fields)
    })
