from flask_restplus import fields
from service.api.v1.models import metadata
from service.api.v1.models import customer


def generate(api):
    return api.model('Root', {
        'metadata': fields.Nested(metadata.generate(api)),
        'customer_list': fields.List(fields.Nested(customer.generate(api))),
        'done_sending': fields.Boolean(default=False)
    })

