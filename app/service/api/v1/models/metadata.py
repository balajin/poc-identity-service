from flask_restplus import fields


def generate(api):
    return api.model('Metadata', {
        'id': fields.Integer(readOnly=True, description='The Identity Service id for the job being modified'),
        'source_system_name': fields.String(readOnly=True, description='Name of the source system requesting work'),
        'source_system_key': fields.Integer(readOnly=True, description='The id for the source system in '
                                                                       'Identity Service', required=True),
        'create_time': fields.Date(readOnly=True, description='Time the job was created'),
        'start_time': fields.Date(readOnly=True, description='Time the job was started'),
        'end_time': fields.Date(readOnly=True, description='Time the job was completed')
    })
