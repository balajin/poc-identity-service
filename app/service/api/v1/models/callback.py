from flask_restplus import fields


def generate(api):
    return api.model('Callback', {
        'type': fields.String(readOnly=True, description='The callback method that should be used'),
        'endpoint': fields.String(readOnly=True, description='The location of the endpoint that should be called')
    })
