from flask_restplus import fields


def generate(api):
    return api.model('Customer', {
        'name': fields.String(readOnly=True, description='The name of the customer'),
        'source_system_customer_id': fields.Integer(readOnly=True, description='The id used by the source system to '
                                                                               'identify the customer'),
        'surrogate_key': fields.Integer(readOnly=True, description='The surrogate key provided by Identity Service'),
        'modified_date': fields.Date
    })
