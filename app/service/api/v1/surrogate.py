import json
import os

from flask import after_this_request, send_file
from flask import request
from flask_restplus import Namespace, Resource, reqparse

from config import title
from service import logger
from service.api.v1.models import surrogate
from service.api.v1.controller.common import get_connection
from service.api.v1.controller.surrogate import get_surrogate, create_surrogate, upsert_surrogate, delete_surrogate
from service.api.v1.controller.surrogate_batch import get_job, create_job, update_job


api = Namespace(
    'surrogate',
    description='Version 1 of {title} API'.format(title=title),
)

surrogate_model = surrogate.generate(api)
surrogate_response = surrogate.generate_response(api)

batch_surrogate_model = surrogate.generate_batch(api)
batch_surrogate_response = surrogate.generate_batch_response(api)


@api.route('/', methods=['GET', 'POST'])
class Surrogate(Resource):
    """Shows a list of all surrogates, and lets you POST to add new surrogates"""

    def get(self):
        """Return list of surrogates"""
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        source_system_slug = request.args.get('sourceSystemSlug')
        message, code = get_surrogate(connection, source_system_slug=source_system_slug)
        connection.close()
        return {"surrogates": message}, code

    @api.expect(surrogate_model)
    @api.marshal_with(surrogate_response)
    def post(self):
        """
        Create new surrogate

        Validation is run against the payload before attempting to insert into the database

        Required Items:
            - `sourceSystemSlug` - internal Platform Identity ID associated with the team or project making the request
            - `sourceSystemRecordId` - how the source system identifies the record being passed to Platform Identity
            - `fiId` - client ID of the record (use 0 if not applicable)
            - (`firstName` OR `lastName`) OR `fullName` - the record will need a name associated with it, a combination of first and last OR a full name field is accepted
            - address* OR `phone` OR `email` - *address is acceptable when it has all of the following fields `address1`, `city`, `state`, and `zip` fields

        Any missing requirements will result in a 400 error with the specified missing data
        Bad data will result in a 400
        Creating a duplicate will return a 409
        Anyhthing else, 500
        """
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        message, code = create_surrogate(api.payload, connection)
        connection.close()
        return message, code, generate_response_header_on_new_item(message, code, 'surrogateId')


@api.route('/<int:surrogate_id>', methods=['GET', 'PUT'])
@api.response(404, 'Surrogate not found')
class Surrogate(Resource):
    """Single surrogate"""
    def get(self, surrogate_id):
        """Fetch a given surrogate"""
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        message, code = get_surrogate(connection, surrogate_id)
        connection.close()
        return message, code

    @api.expect(surrogate_model)
    @api.marshal_with(surrogate_response)
    def put(self, surrogate_id):
        """Update existing surrogate or create new if not existing"""
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        message, code = upsert_surrogate(surrogate_id, api.payload, connection)
        connection.close()
        return message, code, generate_response_header_on_new_item(message, code, 'surrogateId')

    def delete(self, surrogate_id):
        """Delete specified record"""
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        message, code = delete_surrogate(surrogate_id, connection)
        connection.close()
        return message, code


@api.route('/batch/<int:job_id>', methods=['GET', 'PUT'])
@api.response(404, 'Job not found')
class Surrogate(Resource):
    """Single surrogate"""
    @api.marshal_with(batch_surrogate_response)
    def get(self, job_id):
        """Fetch a given surrogate"""
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        page = int(request.args.get('page', 0))
        per_page = int(request.args.get('perPage', 0))
        paging_only = True if request.args.get('pagingOnly', 'false').lower() == 'true' else False
        args = dict(page=page,
                    per_page=per_page,
                    paging_only=paging_only)
        message, code = get_job(connection, job_id, **args)
        connection.close()
        return message, code

    @api.marshal_with(batch_surrogate_response)
    def put(self, job_id):
        """Update existing job"""
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        action = request.args.get('action')
        message, code = update_job(api.payload, job_id, connection, action)
        connection.close()
        return message, code

    # def delete(self, job_id):
    #     """Delete specified job"""
    #     connection = get_connection(database=os.getenv('DATABASE_NAME'))
    #     message, code = delete_job(job_id, connection)
    #     connection.close()
    #     return message, code

@api.route('/batch/', methods=['POST'])
class Surrogate(Resource):
    @api.expect(batch_surrogate_model)
    @api.marshal_with(batch_surrogate_response)
    def post(self):
        connection = get_connection(database=os.getenv('DATABASE_NAME'))
        data = dict()
        for field in batch_surrogate_model:
            data[field] = batch_surrogate_model[field].default
        data.update(**api.payload)
        message, code = create_job(data, connection)
        connection.close()
        return message, code, generate_response_header_on_new_item(message, code, 'jobId')


def generate_response_header_on_new_item(message, code, key):
    if code == 201:
        headers = {'location': f'{message.get(key)}'}
    else:
        headers = dict()
    return headers


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
