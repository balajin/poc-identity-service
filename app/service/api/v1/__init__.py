from flask import Blueprint
from flask_restplus import Api
from service.api.v1.surrogate import api as surrogate_api


def get_blueprint(title=None, description=None):
    blueprint = Blueprint('v1', __name__, url_prefix='/api/v1')
    api = Api(
        blueprint,
        title='{title} API'.format(title=title),
        version='1.0',
        description=description
    )
    api.add_namespace(surrogate_api)

    return blueprint
