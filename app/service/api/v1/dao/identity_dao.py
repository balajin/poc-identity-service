class JobDAO(object):
    """Example DAO that can be used to create a list of jobs in memory."""
    def __init__(self, api):
        self.api = api
        self.counter = 0
        self.jobs = []

    def get_ids(self):
        """Returns a list of job ids"""
        return [job['id'] for job in self.jobs]

    def get(self, job_id):
        """Returns the job being requested"""
        for job in self.jobs:
            if job['id'] == job_id:
                return job
        self.api.abort(404, "Job {} doesn't exist".format(job_id))

    def create(self, payload):
        """Creates an empty job and returns it's metadata"""
        job = {}
        job['id'] = self.counter = self.counter + 1
        job['source_system_key'] = payload.get('source_system_key')
        job['source_system_name'] = self.getattr(payload, 'source_system_name')
        job['callback'] = payload.get('callback')
        job['customer_list'] = []
        self.jobs.append(job)
        return dict({'metadata': job})

    def update(self, job_id, payload):
        """Extends the existing jobs customer list with a new list of customers"""
        job = self.get(job_id)
        job['customer_list'].extend(payload['customer_list'])
        return True

    def delete(self, job_id):
        """Deletes a job from the list"""
        job = self.get(job_id)
        self.jobs.remove(job)

    def getattr(self, payload, target):
        # Flask RestPlus marshals any data being sent with 'string' for items that are strings. This compensates for
        # the lost of being able to use get with a default value. The end result will be to query the database instead
        # of a getter for each item. This getter method of doing it is temporary for the POC.

        functions = {
            'source_system_name': self.get_source_name
        }
        return functions[target](0) if payload.get(target) == 'string' else payload.get(target)

    def get_source_name(self, source_id):
        return 'MAP' if source_id else 'MAP'
