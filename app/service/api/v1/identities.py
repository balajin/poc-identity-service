import json

from flask import request
from flask_restplus import Namespace, Resource

from service.api.v1.dao.identity_dao import JobDAO
from service.api.v1.models import assignment

api = Namespace(
    'identities',
    description='Version 1 of Identity Service API',
)

model = assignment.generate(api)

DAO = JobDAO(api)


@api.route('/', methods=['GET', 'POST'])
class JobList(Resource):
    """Shows a list of all assignment jobs, and lets you POST to add new jobs"""

    def get(self):
        """List all surrogate key assignment jobs"""
        return DAO.get_ids()

    @api.expect(model['metadata'])
    @api.marshal_with(model)
    def post(self):
        """Create a new job for assigning surrogate keys"""
        data = load_data(request.data)
        return DAO.create(data)


@api.route('/<int:job_id>', methods=['GET', 'DELETE', 'PUT'])
@api.response(404, 'Job not found')
@api.param('id', 'The assignment job identifier')
class AssignmentJob(Resource):
    """Show a single assignment job and lets you delete it"""
    def get(self, job_id):
        """Fetch a given assignment job"""
        DAO.get(job_id)
        return "Exists", 200

    @api.response(204, 'Job deleted')
    def delete(self, job_id):
        """Delete an assignment job given its identifier"""
        DAO.delete(job_id)
        return 'Job deleted', 204

    @api.expect(model)
    @api.marshal_with(model)
    def put(self, job_id):
        """Append work to an assignment job given its identifier"""
        data = load_data(request.data)
        if DAO.update(job_id, data):
            return '', 202


def load_data(data):
    try:
        return json.loads(data)
    except json.JSONDecodeError:
        raise AttributeError


if __name__ == '__main__':

    app.run(host='0.0.0.0', port=5000)
