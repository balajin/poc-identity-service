from service import global_secrets
import os
import re
from kasasa_common import parse_db_envvars, get_mysql_connection


# compile up top for faster execution
first_cap_re = re.compile('(.)([A-Z0-9][a-z]+)')
all_cap_re = re.compile('([a-z])([A-Z0-9])')

def get_connection(database=False):
    db_password_key = os.getenv('DATABASE_PASSWORD_KEY')
    db_password = global_secrets.get(db_password_key, '')
    db_dict = parse_db_envvars(secrets={db_password_key: db_password}, database=database)
    return get_mysql_connection(**db_dict,
                                dict_cursor=True)

def convert_camel_to_snake(string):
    step_1 = first_cap_re.sub(r'\1_\2', string)
    return all_cap_re.sub(r'\1_\2', step_1).lower()


def convert_snake_to_camel(string):
    components = string.split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return components[0] + ''.join(x.title() for x in components[1:])


def convert_keys(func, the_dict):
    if not isinstance(the_dict, dict):
        return the_dict

    new_dict = dict()
    for key in the_dict:
        new_key = func(key)
        new_dict[new_key] = the_dict[key]
    return new_dict

def format_sql_insert_fields(data, known_fields):
    """
    takes valid (known) fields and creates the
    field names for the insert statement
    """
    output = []
    for field in data:
        if field in known_fields:
            output.append(field)

    return ', '.join(output)


def format_sql_insert_values(data, known_fields):
    """
    takes valid (known) fields and creates formatting values to be replaced by sql later
    e.g.: turns data = {'field_name1': 'some value', 'field_name2': 'some other value', ...}
            into '%(field_name1)s, %(field_name2)s, ...'
    """
    output = []
    for field in data:
        if field in known_fields:
            output.append("".join(["%(", field, ")s"]))

    return ', '.join(output)


def format_multiple_sql_insert_fields(known_fields):
    """
    takes valid (known) fields and creates the
    field names for the insert statement
    """
    output = []
    for field in known_fields:
        output.append(field)

    return ', '.join(output)

def format_multiple_sql_insert_values(data, known_fields):
    """
    takes valid (known) fields and creates formatting values to be replaced by sql later
    e.g.: turns data = {'field_name1': 'some value', 'field_name2': 'some other value', ...}
            into '%(field_name1)s, %(field_name2)s, ...'
    """
    output = []
    for i, record in enumerate(data):
        full_values = []
        for field in known_fields:
            full_values.append("".join(["%(", str(i), field, ")s"]))
        output.append(''.join(["(", ', '.join(full_values), ")"]))
    return ', '.join(output)


def format_sql_update_fields(data, known_fields):
    """
    takes valid (known) fields and creates formatting values to be replaced by sql later
    e.g.: turns data = {'field_name1': 'some value', 'field_name2': 'some other value', ...}
            into 'field_name1 = %(field_name1)s, field_name2 = %(field_name2)s, ...'
    """
    output = []
    for field in data:
        field = convert_camel_to_snake(field)
        if field in known_fields:
            output.append("".join([field, " = ", "%(", field, ")s"]))

    return ', '.join(output)


def get_known_fields(model):
    return {convert_camel_to_snake(y) for y in model}


def format_output(result, code):
    return convert_keys(convert_snake_to_camel, result), code


def format_input(in_dict):
    return convert_keys(convert_camel_to_snake, in_dict)
