from collections import OrderedDict
from math import ceil

from service.api.v1.models import surrogate
from service import logger
from config import title
from .common import convert_camel_to_snake, convert_snake_to_camel, convert_keys, \
    get_known_fields, format_sql_update_fields, format_sql_insert_values, \
    format_sql_insert_fields, format_multiple_sql_insert_fields, \
    format_multiple_sql_insert_values, format_output, format_input

from flask_restplus import Namespace
from pymysql import IntegrityError, DataError


api = Namespace(
    'surrogate',
    description='Version 1 of {title} API'.format(title=title),
)

batch_model = surrogate.generate_batch(api)
batch_fields = get_known_fields(batch_model)
surrogate_model = surrogate.generate(api)
surrogate_fields = get_known_fields(surrogate_model)


def get_job(connection, job_id, paging_only=False, page=None, per_page=None):
    result = dict()
    code = 500
    base_get_job_query = """
        SELECT *
        FROM batch_jobs
        WHERE job_id = %(job_id)s;
    """
    with connection.cursor() as cursor:
        cursor.execute(base_get_job_query, args=dict(job_id=job_id))
        query_result = cursor.fetchall()
        logger.debug(query_result)
    if query_result:
        result = query_result[0]
        code = 200
        if result.get('status', '').lower() == 'completed':
            count = get_count(connection, job_id)
            result['paging'] = {'totalResults': count}
            if page and per_page:
                try:
                    result['records'] = get_page_result(connection, job_id, count, page, per_page)
                except Exception as e:
                    logger.error((f'job_id={job_id}', e))
                    result['records'] = []
                    result['messages'] = [f'page {page} unavailable']
    else:
        result['messages'] = ['job not found']
        code = 404
    return format_output(result, code)


def update_job(data, job_id, connection, action):
    expected_actions = ['append', 'start', 'cancel']
    data = format_input(data)
    code = 500
    result = dict()
    existing_data, existing_code = get_job(connection, job_id)
    existing_data = format_input(existing_data)

    if existing_code != 200:
        return dict(), 404

    if action and action in expected_actions:
        if action == 'append':
            message, code = append_to_job(data, job_id, existing_data['source_system_slug'], connection)
        elif action == 'start':
            message, code = start_job(job_id, connection)
        elif action == 'cancel':
            message, code = cancel_job(job_id, connection)
        result, _ = get_job(connection, job_id)
        result['messages'] = result.get('messages', []) + [message]
        logger.debug(result)
    else:
        result = existing_data
        result['messages']: result.get('messages', []) + \
            [f'no action parameter found to take; expecting action in {expected_actions}']
        code = 400
    return format_output(result, code)


def create_job(data, connection):
    data = format_input(data)
    data['status'] = 'CREATED'
    logger.debug(data)
    base_insert_query = """
        INSERT INTO batch_jobs (job_type, source_system_slug, status)
        VALUES (%(job_type)s, %(source_system_slug)s, %(status)s)
    """
    get_job_query = """
        SELECT *
        FROM batch_jobs
        WHERE job_type = %(job_type)s
            AND source_system_slug = %(source_system_slug)s
        ORDER BY datetime_created DESC
        LIMIT 1;
    """

    valid, errors = batch_create_is_valid(data, connection)
    if valid:
        with connection.cursor() as cursor:
            logger.debug(base_insert_query)
            cursor.execute(base_insert_query, args=data)
            connection.commit()
            cursor.execute(get_job_query, args=data)
            result = cursor.fetchall()[0]
            logger.debug(result)
            code = 201
    else:
        result = dict(messages=errors)
        code = 400
    return format_output(result, code)
# END API METHODS


# HELPER METHODS
def get_count(connection, job_id):
    count = 0
    base_get_count_for_job_query = """
        SELECT COUNT(1) as `count` FROM surrogates_job_%(job_id)s;
    """
    with connection.cursor() as cursor:
        cursor.execute(base_get_count_for_job_query, args=dict(job_id=job_id))
        count = cursor.fetchall()[0]['count']
    return count


def get_page_result(connection, job_id, count, page, per_page):
    result = []
    base_get_result_query = """
        SELECT s.*
        FROM surrogates_job_%(job_id)s job
            INNER JOIN surrogates s
                ON s.source_system_slug = job.source_system_slug
                AND s.source_system_record_id = job.source_system_record_id
        WHERE job.marker BETWEEN %(marker_start)s AND %(marker_end)s
    """
    try:
        marker_start, marker_end = get_markers(count, page, per_page)
    except ValueError as ve:
        raise ve
    with connection.cursor() as cursor:
        cursor.execute(base_get_result_query, args=dict(job_id=job_id,
                                                        marker_start=marker_start,
                                                        marker_end=marker_end))
        query_result = cursor.fetchall()
        for record in query_result:
            result.append(convert_keys(convert_snake_to_camel, record))
    return result


def get_markers(count, page, per_page):
    end = min([(page * per_page), count])
    start = (page * per_page) - (per_page - 1)
    if start > end:
        # most likely this is for a page outside of the range of the data set
        raise ValueError("pagination out of bounds")
    return start, end


def has_open_batch(data, connection):
    base_job_query = """
        SELECT job_id
        FROM batch_jobs
        WHERE status NOT IN  ('COMPLETED', 'CANCELLED', 'FAILED')
            AND source_system_slug = %(source_system_slug)s
            AND job_type = %(job_type)s;
    """
    with connection.cursor() as cursor:
        cursor.execute(base_job_query, args=data)
        results = cursor.fetchall()
    return not bool(len(results))


def has_job_type(data):
    return bool(data.get('job_type'))


def batch_create_is_valid(data, connection):
    valid = False
    errors = []

    valid_source_system_slug = has_source_system_slug(data)
    valid_job_type = has_job_type(data)

    valid = all(
        [valid_source_system_slug,
         valid_job_type,]
    )

    if not valid:
        if not valid_open_batch:
            errors.append("has_open_batch failed")
        if not valid_source_system_slug:
            errors.append("valid_source_system_slug failed")
        if not valid_job_type:
            errors.append("valid_job_type failed")

    return valid, errors


def has_source_system_slug(data):
    return bool(data.get('source_system_slug', ''))


def append_to_job(data, job_id, source_system_slug, connection):
    find_table_query = """
        SHOW TABLES LIKE 'surrogates_job_%(job_id)s';
    """
    create_table_query = """
        CREATE TABLE IF NOT EXISTS surrogates_job_%(job_id)s LIKE surrogates_batch_template;
    """
    create_rejections_table_query = """
        CREATE TABLE IF NOT EXISTS surrogates_job_%(job_id)s_rejections LIKE surrogates_batch_template;
    """
    alter_rejections_table_query = """
        ALTER TABLE surrogates_job_%(job_id)s_rejections ADD COLUMN reason VARCHAR(255);
    """
    with connection.cursor() as cursor:
        cursor.execute(find_table_query, args=dict(job_id=job_id))
        table = cursor.fetchall()
        if not len(table):
            cursor.execute(create_table_query, args=dict(job_id=job_id))
            cursor.execute(create_rejections_table_query, args=dict(job_id=job_id))
            cursor.execute(alter_rejections_table_query, args=dict(job_id=job_id))
            connection.commit()
            cursor.execute(find_table_query, args=dict(job_id=job_id))
            table = cursor.fetchall()
    # the get table query makes a super weird alias... so just get the first key and use it
    table_name = table[0].get([x for x in table[0]][0])
    message, code = append_to_table(data['records'], job_id, source_system_slug, table_name, connection)
    return message, code


def append_to_table(records, job_id, source_system_slug, table, connection):
    code = 500
    message = 'unable to append to job table'
    try:
        update_status(connection, job_id, 'APPENDING')
        batch_size = 250
        data_count = len(records)
        base_insert = """
            INSERT INTO %(table)s (%(fields)s)
            VALUES %(multi_values)s
        """
        with connection.cursor() as cursor:
            pages = ceil(data_count / batch_size)
            for page in range(1, pages + 1):
                start_index, end_index = get_markers(data_count, page, batch_size)
                # decrementing start for 0-based indexes, NOT decrementing end for slicing
                start_index -= 1

                page_records = records[start_index:end_index]
                converted_records = []
                for record in page_records:
                    converted_records.append(convert_keys(convert_camel_to_snake, record))

                replaces_mes = dict(table=table,
                                    fields=format_multiple_sql_insert_fields(surrogate_fields),
                                    multi_values=format_multiple_sql_insert_values(converted_records, surrogate_fields))
                sql = base_insert % replaces_mes

                # flatten the records into a single dict so that all the values are at the fore
                replace_records = dict()
                for i, record in enumerate(converted_records):
                    for field in surrogate_fields:
                        if field == 'source_system_slug':
                            replace_records[f'{i}{field}'] = source_system_slug
                        else:
                            replace_records[f'{i}{field}'] = record.get(field)

                cursor.execute(sql, args=replace_records)
                connection.commit()
                code = 200
                message = 'ok'
    except IntegrityError as ie:
        logger.error((f'job_id={job_id}', ie))
        code = 409
        message = 'duplicate slug/record id found in data set'
    except DataError as de:
        logger.error((f'job_id={job_id}', de))
        message = 'data does not conform to table types/sizes'
        code = 400
    except Exception as e:
        logger.error((f'job_id={job_id}', e))
    finally:
        return message, code


def start_job(job_id, connection):
    message = 'job not valid to start'
    code = 500
    # validate data
    valid = has_appending_status(job_id, connection)
    if valid:
        try:
            # update the job to be running
            update_status(connection, job_id, 'RUNNING')
            # check for existing slug/record_id pairs in data and remove
            quarantine_existing_surrogates(job_id, connection)
            # check for data that does not have all the requirements for insertion and remove
            quarantine_invalid_data(job_id, connection)
            # update markers
            update_markers(job_id, connection)
            # insert good records to surrogates
            batch_populate_surrogates(job_id, connection)
            # update the job to be completed
            update_status(connection, job_id, 'COMPLETED')
            code = 200
            message = 'job complete'
        except IntegrityError as ie:
            logger.error((f'job_id={job_id}', ie))
            update_status(connection, job_id, 'FAILED')
            code = 409
            message = 'duplicate surrogate would be created with data set'
        except DataError as de:
            logger.error((f'job_id={job_id}', de))
            update_status(connection, job_id, 'FAILED')
            code = 400
            message = 'data set does not conform to table types/sizes'
        except Exception as e:
            logger.error((f'job_id={job_id}', e))
            update_status(connection, job_id, 'FAILED')

    return message, code


def has_appending_status(job_id, connection):
    result, code = get_job(connection, job_id)
    result = format_input(result)
    if code == 200 and result.get('status').lower() in ['appending']:
        return True
    else:
        return False


def quarantine_existing_surrogates(job_id, connection):
    select_query = """
        INSERT INTO surrogates_job_%(job_id)s_rejections
        SELECT job.*, 'existing surrogate' as reason FROM surrogates_job_%(job_id)s job
            INNER JOIN surrogates s
                ON s.source_system_slug = job.source_system_slug
                AND s.source_system_record_id = job.source_system_record_id;
    """
    delete_query = """
        DELETE job.* FROM surrogates_job_%(job_id)s job
            INNER JOIN surrogates s
                ON s.source_system_slug = job.source_system_slug
                AND s.source_system_record_id = job.source_system_record_id;
    """
    with connection.cursor() as cursor:
        cursor.execute(select_query, args=dict(job_id=job_id))
        cursor.execute(delete_query, args=dict(job_id=job_id))
        connection.commit()


def quarantine_invalid_data(job_id, connection):
    select_query = """
        INSERT INTO surrogates_job_%(job_id)s_rejections
        SELECT *, 'invalid data' as reason FROM surrogates_job_%(job_id)s
        WHERE CONCAT(COALESCE(first_name, ''), COALESCE(last_name, ''), COALESCE(full_name, '')) = ''
            OR (CONCAT(COALESCE(address_1, ''), COALESCE(city, ''), COALESCE(state, ''), COALESCE(zip, '')) = ''
                    AND COALESCE(email, '') = '' AND COALESCE(phone, '') = '')
            OR fi_id < 0;
    """
    delete_query = """
        DELETE FROM surrogates_job_%(job_id)s
        WHERE CONCAT(COALESCE(first_name, ''), COALESCE(last_name, ''), COALESCE(full_name, '')) = ''
            OR (CONCAT(COALESCE(address_1, ''), COALESCE(city, ''), COALESCE(state, ''), COALESCE(zip, '')) = ''
                    AND COALESCE(email, '') = '' AND COALESCE(phone, '') = '')
            OR fi_id < 0
    """
    with connection.cursor() as cursor:
        cursor.execute(select_query, args=dict(job_id=job_id))
        cursor.execute(delete_query, args=dict(job_id=job_id))
        connection.commit()


def update_markers(job_id, connection):
    variable_query = "SET @row_number = (SELECT 0);"
    update_query = """
        UPDATE surrogates_job_%(job_id)s
        SET marker = (@row_number:=@row_number + 1)
    """
    with connection.cursor() as cursor:
        cursor.execute(variable_query)
        cursor.execute(update_query, args=dict(job_id=job_id))
        connection.commit()


def batch_populate_surrogates(job_id, connection):
    insert_query = """
        INSERT INTO surrogates (source_system_slug, source_system_record_id,
            fi_id, full_name, first_name, last_name, middle_name, suffix, address_1,
            address_2, city, state, zip, zip_extended, email, phone,
            source_system_record_created_date, source_system_record_last_updated_date)
        SELECT source_system_slug, source_system_record_id, fi_id, full_name, first_name,
            last_name, middle_name, suffix, address_1, address_2, city, state, zip,
            zip_extended, email, phone, source_system_record_created_date,
            source_system_record_last_updated_date
        FROM surrogates_job_%(job_id)s
    """
    with connection.cursor() as cursor:
        cursor.execute(insert_query, args=dict(job_id=job_id))
        connection.commit()


def cancel_job(job_id, connection):
    update_status(connection, job_id, 'CANCELLED')
    return 'marked as cancelled', 200


def update_status(connection, job_id, status):
    base_update = """
        UPDATE batch_jobs
        SET status = %(status)s,
            datetime_last_updated = CURRENT_TIMESTAMP()
            %(completed)s
        WHERE job_id = %(job_id)s
    """
    replace_mes = dict(status='%(status)s',
                       completed='' if status.lower() != 'completed' else ', datetime_completed = CURRENT_TIMESTAMP()',
                       job_id='%(job_id)s')
    data = dict(status=status, job_id=job_id)
    sql = base_update % replace_mes
    logger.debug((status, job_id, sql))
    with connection.cursor() as cursor:
        cursor.execute(sql, args=data)
        connection.commit()
# END HELPER METHODS
