from collections import OrderedDict

from service.api.v1.models import surrogate
from service import logger
from config import title
from .common import convert_camel_to_snake, convert_snake_to_camel, convert_keys, \
    get_known_fields, format_sql_update_fields, format_sql_insert_values, \
    format_sql_insert_fields, format_output

from flask_restplus import Namespace

from pymysql import IntegrityError, DataError


api = Namespace(
    'surrogate',
    description='Version 1 of {title} API'.format(title=title),
)

surrogate_model = surrogate.generate(api)
surrogate_fields = get_known_fields(surrogate_model)

# API FUNCTIONS
def get_surrogate(connection, surrogate_id=None, internal=False, source_system_slug=None):
    base_query = 'SELECT * FROM surrogates'
    try:
        if surrogate_id:
            surrogate_id = int(surrogate_id)
            logger.debug(f"Getting surrogate {surrogate_id}")
            sql = ' '.join([base_query, 'WHERE surrogate_id = %s;'])
            logger.debug(sql)
            result = None
            with connection.cursor() as cursor:
                cursor.execute(sql, args=(surrogate_id,))
                result = cursor.fetchall()
            if result:
                code = 200
                result = result[0]
                if internal:
                    return result
            else:
                code = 404
        else:
            logger.debug(f"Getting all surrogates")
            if source_system_slug:
                sql = ' '.join([base_query, 'WHERE source_system_slug = %(source_system_slug)s;'])
            else:
                sql = base_query
            with connection.cursor() as cursor:
                logger.debug(sql)
                cursor.execute(sql, args=dict(source_system_slug=source_system_slug))
                result = cursor.fetchall()
                logger.debug(result)
                new_result = []
                for surrogate in result:
                    new_result.append(convert_keys(convert_snake_to_camel, surrogate))
                result = new_result
                code = 200
    except ValueError as ve:
        logger.error((f'source_system_slug={source_system_slug}', ve))
        code = 400
        result = {"messages": [str(ve.args)]}
    except Exception as e:
        logger.error((f'source_system_slug={source_system_slug}', e))
        result = {"messages": [str(e.__class__.__name__), str(e.args)]}
        code = 500
    finally:
        return format_output(result, code)


def create_surrogate(data, connection):
    logger.debug(f"Creating new surrogate based on data: {data}")
    data = convert_keys(convert_camel_to_snake, data)
    logger.debug(f"converted to: {data}")
    code = 500
    result = dict()
    # OrderedDict so the fields and values will line up consistently
    data = OrderedDict(data)

    base_insert_query = """
        INSERT INTO surrogates (%(fields)s)
        VALUES (%(values)s);
    """
    base_find_query = """
        SELECT surrogate_id
        FROM surrogates
        WHERE source_system_slug = %(source_system_slug)s
            AND source_system_record_id = %(source_system_record_id)s;
    """

    valid, errors = surrogate_is_valid(data)
    if valid:
        # run sql to populate surrogate
        try:
            with connection.cursor() as cursor:
                replace_mes = dict(fields=format_sql_insert_fields(data, surrogate_fields),
                                   values=format_sql_insert_values(data, surrogate_fields))
                sql = base_insert_query % replace_mes
                logger.debug(sql)
                cursor.execute(sql, args=data)
                cursor.execute(base_find_query, args=data)

                result = cursor.fetchall()
                connection.commit()

                result = {
                    "source_system_record_id": data['source_system_record_id'],
                    "source_system_slug": data['source_system_slug'],
                    "surrogate_id": result[0]['surrogate_id']
                }
                code = 201
        except IntegrityError as ie:
            logger.error((f'source_system_slug={source_system_slug}', ie))
            logger.info("returning request with existing surrogate information")
            with connection.cursor() as cursor:
                cursor.execute(base_find_query, args=data)
                result = cursor.fetchall()[0]
            code = 409
            result["source_system_record_id"] = data["source_system_record_id"]
            result["messages"] = ["unable to create surrogate as it already exists"]
        except DataError as de:
            logger.error((f'source_system_slug={source_system_slug}', de))
            code = 400
            result = {"messages": ["check the data inside payload conforms to schema specifications"]}
        except Exception as e:
            logger.error((f'source_system_slug={source_system_slug}', e))
            code = 500
            result = {"messages": ["something unexpected occurred"]}
        finally:
            return format_output(result, code)
    else:
        return format_output(dict(messages=errors), 400)


def upsert_surrogate(surrogate_id, data, connection):
    logger.debug(f"Updating surrogate {surrogate_id} with data {data}")
    data = convert_keys(convert_camel_to_snake, data)
    logger.debug(f"converted to: {data}")
    code = 500
    result = dict()

    existing_data = convert_keys(convert_camel_to_snake,
                                 get_surrogate(connection, surrogate_id=surrogate_id, internal=True)[0])
    logger.debug(existing_data)

    if not existing_data:
        # break off this execution and try to insert the item as a new record
        return create_surrogate(data, connection)

    proposed_data = {**existing_data, **data}
    proposed_data['surrogate_id'] = surrogate_id
    logger.debug(proposed_data)

    if proposed_data == existing_data:
        return format_output(dict(surrogateId=surrogate_id,
                    sourceSystemSlug=proposed_data['source_system_slug'],
                    sourceSystemRecordId=proposed_data['source_system_record_id'],
                    messages=["no changes to apply"]), 200)
    if proposed_data.get('source_system_slug') != existing_data.get('source_system_slug') or \
            proposed_data.get('source_system_record_id') != existing_data.get('source_system_record_id'):
        return format_output(dict(messages=['sourceSystemSlug or sourceSystemRecordId would be altered. ' \
                                            'These fields must remain immutable.']), 400)

    base_update_query = """
        UPDATE surrogates
        SET %(updates)s
        WHERE surrogate_id = %(surrogate_id)s;
    """

    valid, errors = surrogate_is_valid(proposed_data)
    if valid:
        try:
            replace_mes = dict(updates=format_sql_update_fields(proposed_data, surrogate_fields),
                               surrogate_id='%(surrogate_id)s')
            sql = base_update_query % replace_mes
            logger.debug(sql)
            with connection.cursor() as cursor:
                cursor.execute(sql, args=proposed_data)
            connection.commit()
            code = 200
            result = proposed_data
        except IntegrityError as ie:
            logger.error(ie)
            code = 409
            result = {"messages": ["if you reached this error, you must be trying to change a " \
                        "record's id or source system id or both and it conflicts with another record"]}
        except DataError as de:
            logger.error(de)
            logger.error((f'source_system_slug={proposed_data["source_system_slug"]}', de))
            code = 400
            result = {"messages": ["check the data inside payload conforms to schema specifications"]}
        except Exception as e:
            logger.error((f'source_system_slug={proposed_data["source_system_slug"]}', e))
            code = 500
            result = {"messages": ["something unexpected occurred"]}
        finally:
            return format_output(result, code)
    else:
        return format_output(dict(messages=errors), 400)


def delete_surrogate(surrogate_id, connection):
    logger.debug(f"Deleting surrogate {surrogate_id}")
    code = 500
    result = dict()

    existing_data = get_surrogate(connection, surrogate_id=surrogate_id, internal=True)[0]
    logger.debug(existing_data)

    if not existing_data:
        return format_output({"messages": ["no object found"]}, 404)

    sql = """DELETE FROM surrogates WHERE surrogate_id = %s;"""
    try:
        with connection.cursor() as cursor:
            cursor.execute(sql, args=(surrogate_id,))
        connection.commit()
        result = {"messages": ["record deleted"]}
        code = 200
    except Exception as e:
        logger.error((f'surrogate_id={surrogate_id}', e))
        code = 500
        result = {"messages": ["something unexpected occurred"]}
    finally:
        return format_output(result, code)
# END API FUNCTIONS


# HELPER FUNCTIONS
def has_source_system_slug(data):
    return bool(data.get('source_system_slug', ''))


def has_fi_id(data):
    valid = False

    try:
        valid = int(data.get('fi_id', -1)) >= 0
    except Exception as e:
        pass
    finally:
        return valid


def has_source_system_record_id(data):
    return bool(data.get('source_system_record_id'))


def has_first_name_last_name_or_full_name(data):
    return any(
        [
            data.get('first_name'),
            data.get('last_name'),
            data.get('full_name'),
        ]
    )


def has_address_or_phone_or_email(data):
    return any(
        [
            all(
                [
                    data.get('address_1', ''),
                    data.get('city', ''),
                    data.get('state', ''),
                    data.get('zip', ''),
                ]
            ),
            data.get('phone'),
            data.get('email'),
        ]
    )


def surrogate_is_valid(data):
    errors = []

    # assess individual rules
    valid_source_system_slug = has_source_system_slug(data)
    valid_source_system_record_id = has_source_system_record_id(data)
    valid_fi_id = has_fi_id(data)
    valid_first_name_last_name_or_full_name = has_first_name_last_name_or_full_name(data)
    valid_address_or_phone_or_email = has_address_or_phone_or_email(data)

    # check all rules are met
    valid_data = all(
        [
            valid_source_system_slug,
            valid_source_system_record_id,
            valid_fi_id,
            valid_first_name_last_name_or_full_name,
            valid_address_or_phone_or_email,
        ]
    )

    # append all errors
    if not valid_data:
        if not valid_source_system_slug:
            logger.debug('has_source_system_slug failed')
            errors.append('has_source_system_slug failed')
        if not valid_source_system_record_id:
            logger.debug('has_source_system_record_id failed')
            errors.append('has_source_system_record_id failed')
        if not valid_fi_id:
            logger.debug('has_fi_id failed')
            errors.append('has_fi_id failed')
        if not valid_first_name_last_name_or_full_name:
            logger.debug('has_first_name_last_name_or_full_name failed')
            errors.append('has_first_name_last_name_or_full_name failed')
        if not valid_address_or_phone_or_email:
            logger.debug('has_address_or_phone_or_email failed')
            errors.append('has_address_or_phone_or_email failed')

    # return result
    return valid_data, errors
# END HELPER FUNCTIONS


if __name__ == "__main__":
    msg, status = get_surrogate('1234')
    print(msg, status)
