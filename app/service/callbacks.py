import requests

from kasasa_common.sns import Sns

"""
This is an example of something we could do with callbacks. The idea being that the consumer would include some 
preferred method and an address and we would call get_callback_function to get the right function to call at the end.
The callbacks themselves would generally be pretty small and data to return could be serialized in any way we needed.
i.e. JSON or XML

I would view this as more of a nice to have or something that could be implemented fairly easily if the ask was ever
there.
"""


def get_callback_function(callback_format: str, params={}):
    if callback_format.lower() == 'sns':
        params["callback"] = sns_callback
    elif callback_format.lower() == 'http':
        params["callback"] = http_callback
    return params


def sns_callback(params):
    sns = Sns(params['topic_arn'])

    params['response'] = sns.publish('Some message here')

    return params


def http_callback(params):
    response = requests.post(params['callback_url'], data=params['data'])

    params['response'] = response
    return params
