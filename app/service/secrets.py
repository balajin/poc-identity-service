from service import logger
from kasasa_common import get_vault_secrets
import os
from dotenv import load_dotenv
from pathlib import Path


def get_secrets(local_secrets={}):
    logger.debug('getting secrets')
    if os.getenv('SKIP_VAULT'):
        env_path = Path('.') / '.env.local'
    else:
        env_path = Path('.') / '.env'
    load_dotenv(dotenv_path=env_path)

    if local_secrets:
        if os.getenv('SKIP_VAULT'):
            local_secrets[os.getenv('DATABASE_PASSWORD_KEY')] = os.getenv('DATABASE_PASSWORD')
        return local_secrets

    vault_config = {
        'VAULT_APPROLE_PATH': os.getenv('VAULT_APPROLE_PATH'),
        'VAULT_APPROLE_SECRET_ID': os.getenv('VAULT_APPROLE_SECRET_ID'),
        'VAULT_SECRET_KEY': os.getenv('VAULT_SECRET_KEY'),
        'CONSUL_ADDRESS': os.getenv('CONSUL_ADDRESS')
    }

    return get_vault_secrets(vault_config=vault_config)
