Flask does not hot swap Python code in this container. A debug container can be used to make this happen later.

### Docker

Prerequisite: Login to Kasasa AWS container repository. This image is built on an image that is located there.

`$ $(aws ecr get-login --registry-ids 304872096477 --region us-west-2 | sed -e 's/-e none/ /g')`

#### Build

`$ docker build -t platform-identity ./app/.`

#### Run

`$ docker run -p 80:80 platform-identity`

### Dockerless

This code does not require docker and can be run by installing the requirements via `pip` (preferably in a virtual environment) and adding the necessary environment variables. This simplifies debugging, greatly.

Install java if not already installed: `brew cask install java`
Install the mysql connector from:
> $ curl -o flyway.tar.gz https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/6.0.4/flyway-commandline-6.0.4-macosx-x64.tar.gz<br/>
> $ tar -C ~/ -xpzf flyway.tar.gz

Create a `.env.local` file at root of project with your database env vars. Example:

> DATABASE_HOST=127.0.0.1<br />
> DATABASE_PORT=3306<br />
> DATABASE_USERNAME=your_user_here      # CHANGE ME<br />
> DATABASE_PASSWORD=your_password_here  # CHANGE ME<br />
> DATABASE_PASSWORD_KEY=platform_identity_password<br />
> DATABASE_NAME=identity<br />
> LIQUIBASE_LOCATION=/usr/local/bin/liquibase<br />
> CHANGE_LOG_PATH=app/database/liquibase/change_log.xml<br />
> LIQUIBASE_CLASS_PATH=/path/to/mysql-connector-java-8.0.17.jar<br />

`SKIP_MIGRATIONS=true SKIP_VAULT=true FLASK_APP=app/run.py PYTHONPATH=./app flask run` will start a local flask server if your IDE does not have a different way of starting it.

Run migrations manually with your items replaced in the below command (YOU'LL PROBABLY NEED TO CHANGE THE USER IN change_log.xml under app/database/liquibase):
> liquibase<br />
>     --changeLogFile=app/database/liquibase/change_log.xml \<br />
>     --username=${DATABASE_USERNAME} \<br />
>     --password=${DATABASE_PASSWORD} \<br />
>     --url=jdbc:mysql://${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}?createDatabaseIfNotExist=true&serverTimezone=UTC \<br />
>     --classpath=/usr/share/java/mysql-connector-java-8.0.17.jar \<br />
>     update<br />

OR

Don't set the SKIP_MIGRATIONS env var and after the flask app is up, call an API and the migrations will run like from the deploy steps: http://localhost:5000/admin/ready
